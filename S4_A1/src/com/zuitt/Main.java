package com.zuitt;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {
    public static void main(String[] args){

        User user1 = new User("Terence Gaffud", 25, "tgaff@mail.com", "Quezon City");

        System.out.println("Hi! I'm " + user1.getName() + ". I'm " + user1.getAge() + " years old. You can reach me via my email: " + user1.getEmail() + ". When I'm off work, I can be found at my house in " + user1.getAddress() + ".");

        Course course1 = new Course();

        Date startDate = new Date(123, 6, 10);
        Date endDate = new Date(123, 6, 14);
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

        course1.setName("MACQ004");
        course1.setDescription("An introduction to Java for career shifters");
        course1.setSeats(30);
        course1.setFee(2000);
        course1.setStartDate(startDate);
        course1.setEndDate(endDate);
        course1.setInstructor(user1);

        System.out.println("Welcome to the course " + course1.getName() + ". This course can be described as " + course1.getDescription() + ". Your instructor for this course is Mr. " + course1.getInstructor().getName() + ". Your start date will be on " + dateFormat.format(course1.getStartDate()) + " and ends on " + dateFormat.format(course1.getEndDate()) + ". The total seats available for this class is " + course1.getSeats() );
    }
}
