package com.zuitt.example;

public class Person implements Actions, Greeting {
    public void sleep(){
        System.out.println("Zzzzzz.....");
    }
    public void run(){
        System.out.println("Running on the road");
    }
    /*
        Mini-Activity
        Create two new actions that a person would do and implement them in the actions class
    */
    public void eat(){
        System.out.println("Crunch!");
    }
    public void sigh(){
        System.out.println("huff...");
    }
    public void morningGreet(){
        System.out.println("Good Morning!");
    }
    public void holidayGreet(){
        System.out.println("Happy Holidays!");
    }
}
