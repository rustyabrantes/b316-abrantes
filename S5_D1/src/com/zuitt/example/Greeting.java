package com.zuitt.example;

public interface Greeting {
    public void morningGreet();
    public void holidayGreet();
}
