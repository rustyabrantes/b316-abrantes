package com.zuitt.activity;

import java.util.Scanner;

public class Activity1 {
    public static void main(String[] args) {
        int year;
        Scanner myObj = new Scanner(System.in);
        System.out.println("Input a year to be checked if a leap year.");
        year = myObj.nextInt();
        if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
            System.out.println(year + " is a leap year");
        } else {
            System.out.println(year + " is not a leap year");
        }
    }

}
