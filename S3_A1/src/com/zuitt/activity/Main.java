package com.zuitt.activity;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);
        System.out.println("Input an integer whose factorial will be computed: ");
        int num = 0;
        try {
            num = myObj.nextInt();
            if(num < 0) throw new Exception("negative");
            if(num == 0 ) throw new Exception(("0, It's factorial is 1"));
        } catch(Exception e) {
            System.out.println("Input is " + e.getMessage());
            System.exit(0);
        }

        // Using while loop
        int answer = 1;
        int counter = 1;

        while(counter <= num) {
            answer *= counter;
            counter++;
        }
        System.out.println("The factorial of " + num + " is " + answer);

        // Using for loop
        int answer2 = 1;

        for (int i = num; i >= 1; i--) {
            answer2 *= i;
        }

        System.out.println("The factorial of " + num + " is " + answer2);


        // Stretch goal
        for (int i = 1; i <= 5; i++) {
            for (int j = 0; j < i; j++) {
                System.out.print("* ");
            }
            System.out.println();
        }
    }

}
