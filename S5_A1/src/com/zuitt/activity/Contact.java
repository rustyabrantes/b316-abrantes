package com.zuitt.activity;

public class Contact {
    // Variables
    private String name;
    private String contactNumber;
    private String homeAddress;
    private String officeAddress;

    // Default constructor
    public Contact(){

    }

    // Parameterized constructor
    public Contact(String name, String contactNumber, String homeAddress, String officeAddress){
        this.name = name;
        this.contactNumber = contactNumber;
        this.homeAddress = homeAddress;
        this.officeAddress = officeAddress;
    }

    // Getters
    public String getName(){
        return name;
    }
    public String getContactNumber(){
        return contactNumber;
    }
    public String getHomeAddress(){
        return homeAddress;
    }
    public String getOfficeAddress(){
        return officeAddress;
    }

    // Setters
    public void setName(String name){
        this.name = name;
    }
    public void setContactNumber(String contactNumber){
        this.contactNumber = contactNumber;
    }
    public void setHomeAddress(String homeAddress){
        this.homeAddress = homeAddress;
    }
    public void setOfficeAddress(String officeAddress){
        this.officeAddress = officeAddress;
    }
}
