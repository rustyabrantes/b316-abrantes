
package com.zuitt.activity;

import java.util.ArrayList;

public class Phonebook {

    // Single variable
    private ArrayList<Contact> contacts = new ArrayList<Contact>();

    // Default constructor
    public Phonebook(){

    }

    // Parameterized constructor
    public Phonebook(ArrayList<Contact> contacts){
        this.contacts = contacts;
    }

    // Getter
    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    // Setter
    public void setContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

}
