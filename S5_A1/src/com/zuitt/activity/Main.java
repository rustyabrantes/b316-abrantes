package com.zuitt.activity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Main {
    public static void main(String[] args){
        Phonebook phonebook = new Phonebook();
        Contact contact1 = new Contact("John Doe", "+639152468596 \n+639228547963", "Quezon City", "Makati City");
        Contact contact2 = new Contact("Jane Doe", "+639162148573 \n+639173698541", "Caloocan City", "Pasay City");

        ArrayList<Contact> contactList = new ArrayList<>();
        contactList.add(contact1);
        contactList.add(contact2);

        phonebook.setContacts(contactList);

        if (phonebook.getContacts().isEmpty()) {
            System.out.println("Phonebook is empty");
        } else {
            for (Contact contact : phonebook.getContacts()) {
                System.out.println(contact.getName());
                System.out.println("--------------------");
                System.out.println(contact.getName() + " has the following registered numbers:");
                System.out.println(contact.getContactNumber());
                System.out.println("------------------------------");
                System.out.println(contact.getName() + " has the following registered addresses:");
                System.out.println("my home in " + contact.getHomeAddress());
                System.out.println("my office in " + contact.getOfficeAddress());
                System.out.println("==============================");
            }
        }

        // Stretch Goals

        User user1 = new User("Terrence Gaffud", 25, "tgaff@mail.com", "Quezon City");

        System.out.println("Hi! I'm " + user1.getName() + ". I'm " + user1.getAge() + " yrs old. You can reach me via my email address: " + user1.getEmail() + ". When I'm off work, I can be found at my house in " + user1.getAddress() + "." );

        Course course1 = new Course();

        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy");

        Date startDate = new Date(123, 6,10);
        Date endDate = new Date(123, 6,14);

        course1.setName("MACQ004");
        course1.setDescription("An introduction to Java for career-shifters");
        course1.setSeats(30);
        course1.setFee(2000);
        course1.setStartDate(startDate);
        course1.setEndDate(endDate);
        course1.setInstructor(user1);

        System.out.println("Welcome to the course " + course1.getName() + ". This course can be describe as " + course1.getDescription() + ". Your instructor for this course is "+ user1.getName() + ". With a total of " + course1.getSeats() + " seats available, the fee for this course is " + course1.getFee() + " php. This course will start on " + dateFormat.format(course1.getStartDate()) + " and ends on " + dateFormat.format(course1.getEndDate()) + ". Enjoy!");
    }
}
