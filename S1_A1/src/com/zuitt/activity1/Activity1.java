package com.zuitt.activity1;
import java.util.Scanner;
public class Activity1 {
    public static void main(String[] args){
        Scanner myObj = new Scanner(System.in);
        System.out.println("First Name:");
        String firstName = myObj.nextLine();
        System.out.println("Last Name:");
        String lastName = myObj.nextLine();
        System.out.println("First Subject Grade: ");
        double firstSubject = new Double(myObj.nextLine());
        System.out.println("Second Subject Grade: ");
        double secondSubject = new Double(myObj.nextLine());
        System.out.println("Third Subject Grade: ");
        double thirdSubject = new Double(myObj.nextLine());
        System.out.println("Good day, " + firstName + " " + lastName + ".");
        double average = (firstSubject + secondSubject + thirdSubject)/3;
        System.out.println("Your average grade is: " + Math.round(average));
    }
}
