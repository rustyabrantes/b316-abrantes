package com.zuitt.example;

public class Main {
    public static void main(String[] args){
        /*Car car1 = new Car();
        System.out.println(car1.brand);
        System.out.println(car1.make);
        System.out.println(car1.price);
        car1.make = "Veyron";
        car1.brand = "Bugatti";
        car1.price = 1900000;
        System.out.println(car1.brand);
        System.out.println(car1.make);
        System.out.println(car1.price);

        *//*
            Mini-Activity:

            Create two new instances of the Car class and save it in a variable called car2 and car3 respectively.
            Access the properties of the instance and update its values.
                make = String
                brand = String
                price = int
             You can come up with your own values.
             Print the values of each property of the instance.
        *//*
        // Instance - an object created from a class
        Car car2 = new Car();
        car2.make = "Civic";
        car2.brand = "Honda";
        car2.price = 1500000;
        System.out.println(car2.brand);
        System.out.println(car2.make);
        System.out.println(car2.price);

        Car car3 = new Car();
        car3.make = "Wigo";
        car3.brand = "Toyota";
        car3.price = 700000;
        System.out.println(car3.brand);
        System.out.println(car3.make);
        System.out.println(car3.price);*/
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        Driver driver1 = new Driver( "Alejandro", 25);
        System.out.println(driver1.getName());
        car1.start();
        car2.start();
        car3.start();

        // Property getters
        System.out.println(car1.getMake());
        System.out.println(car2.getMake());

        // Property setters
        car1.setMake("Veyron");
        System.out.println(car1.getMake());

        car2.setMake("Innova");
        System.out.println(car2.getMake());

        // carDriver Getter
        System.out.println(car1.getCarDriver().getName());

        Driver newDriver = new Driver("Antonio", 21);
        car1.setCarDriver(newDriver);

        // Get name of new carDriver
        System.out.println(car1.getCarDriver().getName());
        System.out.println(car1.getCarDriver().getAge());

        System.out.println(car1.getCarDriverName());

        /*
         * Mini-Activity
         * Create a new class called Animal with the following attributes
         * name - string
         * color - string
         *
         * Add constructors, getters and setters for the class.
        */

        Animal animal = new Animal("Clifford", "Red");
        animal.call();

        Dog dog1 = new Dog();
        System.out.println(dog1.getName());
        dog1.call();
        dog1.setName("Hachiko");
        System.out.println(dog1.getName());
        dog1.call();
        dog1.setColor("White");
        System.out.println(dog1.getColor());
        dog1.greet();

        Dog dog2 = new Dog("Mike", "Dark Brown", "Corgi");
        dog2.call();
        System.out.println(dog2.getName());
        System.out.println(dog2.getDogBreed());
        dog2.greet();
    }
}
